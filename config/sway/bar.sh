#!/bin/sh -e

time=$(date +'%b-%d %H:%M')

bat_dir=/sys/class/power_supply/BAT0

read -r cap < "$bat_dir/capacity"
read -r stat < "$bat_dir/status"

snd=$(wpctl get-volume @DEFAULT_AUDIO_SINK@ | cut -d ' ' -f 2,3)

printf "$time | $cap $stat | $snd"
