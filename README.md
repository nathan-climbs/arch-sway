# arch sway

updated April 2024

## theme
catppuccin and tokynight

### tela icons
https://github.com/vinceliuice/Tela-icon-theme

### magnetic gtk
https://github.com/vinceliuice/Magnetic-gtk-theme

### bibata modern ice cursor
https://github.com/ful1e5/Bibata_Cursor

### sddm theme
https://github.com/catppuccin/sddm
