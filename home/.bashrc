# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
    . /etc/bashrc
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

HISTCONTROL=ignoreboth:erasedups
HISTSIZE=1000
HISTFILESIZE=1000
EDITOR=hx

shopt -s \
    autocd \
    globstar \
    checkwinsize \
    cdspell \
    dirspell \
    histappend \
    expand_aliases \
    dotglob \
    gnu_errfmt \
    histreedit \
    nocasematch

bind 'set show-all-if-ambiguous on'
bind 'set colored-stats on'
bind 'set completion-display-width 1'
bind 'TAB:menu-complete'

# prompt
## Reset to normal:
NORM="\033[0m"

## Colors:
CYN="\033[0;36m"
YEL="\033[0;33m"

PS1="\n${CYN}\w\n${YEL}⌁ ${NORM}"

# aliases
alias ls="ls -aFh --color=auto --group-directories-first"
alias cp="cp -i"
alias mv="mv -i"
alias rm="rm -I"
alias rmd="rm -rfI"
alias hx=helix

# git
alias gc='git clone'

#functions

# create a new directory and enter it
mkd() {
  mkdir -p "$1" && cd "$1"
}

# create a new tmux session and use dir name as session name
tn() {
  tmux new -s $(pwd | sed 's/.*\///g')
}
